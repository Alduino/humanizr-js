This is a port of [Humanizer](https://github.com/Humanizr/Humanizer) for Javascript. Humanizer meets all your Javascript
needs for manipulating and displaying strings, enums, dates, times, timespans, numbers and quantities. It is licensed
under the [MIT](https://opensource.org/licenses/MIT) (an OSI approved license).

This library is currently in very early development, so most features of Humanizer aren't supported yet. The features
currently documented in this readme are the ones that are supported.

## Install
You can install Humanizer from NPM:
```shell script
npm i @alduino/humanizer
yarn add @alduino/humanizer
pnpm i @alduino/humanizer
```

## Alternatives
- [humanizer.node](https://www.npmjs.com/package/humanizer.node)
    - Pros: More faithful port of Humanizer.NET, currently supports more features, recently updated
    - Cons: Pollutes prototypes

## Features
### Humanise string
```js
import {humanize} from "@alduino/humanizer/string";
// or
const {humanize} = require("@alduino/humanizer/string");
```

You can call the `humanize()` function to turn an otherwise computerised string into a more readable human-friendly one.

```js
humanize("PascalCaseInputStringIsTurnedIntoSentence") => "Pascal case input string is turned into sentence"
humanize("Underscored_input_string_is_turned_into_sentence") => "Underscored input string is turned into sentence"
humanize("Underscored_input_String_is_turned_INTO_sentence") => "Underscored input String is turned INTO sentence"
```

### Dehumanise string
```js
import {dehumanize} from "@alduino/humanizer/string";
// or
const {dehumanize} = require("@alduino/humanizer/string");
```

Much like you can humanise a computer friendly into human friendly string, you can dehumanise a human friendly string
into a computer friendly one:
```js
dehumanize("Pascal case input string is turned into sentence") => "PascalCaseInputStringIsTurnedIntoSentence"
```

### Transform string
```js
import {transform, toLowerCase} from "@alduino/humanizer/string";
// or
const {transform, toLowerCase} = require("@alduino/humanizer/string");
```

The `transform` function applies some kind of transformation to the input string. There are a few default
implementations:
```js
transform("Sentence casing", toLowerCase) => "sentence casing"
transform("Sentence casing", toSentenceCase) => "Sentence casing"
transform("Sentence casing", toTitleCase) => "Sentence Casing"
transform("Sentence casing", toUpperCase) => "SENTENCE CASING"
```

You can create your own transformers by implementing the `IStringTransformer` interface, which you can import from the
same place.

### Truncate string
```js
import {truncate} from "@alduino/humanizer/string";
// or
const {truncate} = require("@alduino/humanizer/string");
```

You can truncate a string using the `truncate` method:
```js
truncate("Long text to truncate", 10) => "Long text…"
```

By default, the `…` character is used to truncate strings. The advantage of using the `…` character over `...` is that
the former is only a single character long, and thus allows more text to be shown before truncation. If you want, you
can override this with your own truncation string:
```js
truncate("Long text to truncate", 10, "---") => "Long te---"
```

The default truncation strategy, `fixedLength`, is used to truncate the input stringto a specific length, including the
truncation string length. There are two more default truncator strategies available: one for a fixed number of
(alpha-numerical) characters, and one for a fixed number of words. To use a specific truncator when truncating, pass it
after the truncation string (you can use `null` as the truncation string value to use its default):
```js
import {fixedLength, fixedNumberOfCharacters, fixedNumberOfWords} from "@alduino/humanizer/string";

truncate("Long text to truncate", 10, null, fixedLength) => "Long text…"
truncate("Long text to truncate", 6, null, fixedNumberOfCharacters) => "Long t…"
truncate("Long text to truncate", 2, null, fixedNumberOfWords) => "Long text…"
```

You can create your own truncator by implementing the `ITruncator` interfacea from the same import.

There is also an option to choose whether to truncate the string from the beginning (`TruncateFrom.Start`) or the end
(`TruncateFrom.End`). Default is the end as shown in the examples above, however you can truncate from the beginning by
passing `TruncateFrom.Start` as the last parameter.

### Inflector functions
#### Pluralize
```js
import {pluralize} from "@alduino/humanizer/string";
// or
const {pluralize} = require("@alduino/humanizer/string");
```

`Pluralize` pluralises the provided input while taking irregular and uncountable words into consideration:
```js
pluralize("Man") => "Men"
pluralize("string") => "strings"
```

Normally, you would call `pluralize` on a singular word, but if you are unsure you can specify the last parameter as
`false`:
```js
pluralize("Men", false) => "Men"
pluralize("Man", false) => "Men"
pluralize("string", false) => "strings"
```

#### Singularize
See pluralize, but reversed.

#### Adding words
Sometimes, you may need to add a rule from the singularisation/pluralisation vocabulary (the examples below are already
in the default vocabulary):
```js
import {defaultVocabulary} from "@alduino/humanizer/string";

// Adds a word that can't easily be matched using regex
// By default, will be matched as an ending option, so could be `person` or `salesperson`
defaultVocabulary.addIrregular("person", "people");
// To only match whole words, add a parameter `false`:
defaultVocabulary.addIrregular("person", "people", false);

// Adds an uncountable word
defaultVocabulary.addUncountable("fish");

// Adds a rule to the vocabulary that doesn't follow trivial rules
defaultVocabulary.addPlural("bus", "busses");
defaultVocabulary.addPlural("(vert|ind)ices$", "$1ex")
```

### DateTime functions
```js
import humanize from "@alduino/humanizer/dateTime";
// or
const humanize = require("@alduino/humanizer/dateTime");
```

> Todo: Special cases - yesterday, tomorrow

You can humanise a `Date` and get a string cak telling how far back or forward in time that is:
```js
humanize(new Date(Date.now() - 30 * 60 * 60 * 1000 /* 30 hrs */)) => "one day ago"
humanize(new Date(Date.now() - 2 * 60 * 60 * 1000 /* 2 hrs */)) => "two hours ago"

humanize(new Date(Date.now() + 30 * 60 * 60 * 1000 /* 30 hrs */)) => "in one day"
humanize(new Date(Date.now() + 2 * 60 * 60 * 1000 /* 2 hrs */)) => "in two hours"
```

Specify the second parameter as the offset you want to use to the first parameter, defaulting to the current time.

You may also supply your own humanising strategy by implementing the `IDateTimeHumanizeStrategy` interface exported from
the same place. Pass it as the 3rd parameter.

### Number functions
#### Number to words
```js
import {words} from "@alduino/humanizer/number";
// or
const {words} = require("@alduino/humanizer/number");
```

Humanizer can change numbers to words using the `words` function:
```js
words(1) => "one"
words(10) => "ten"
words(11) => "eleven"
words(122) => "one hundred and twenty-two"
words(3501) => "three thousand five hundred and one"
```

#### Number to ordinal words
You can call `words` with `true` as the second parameter to return ordinal words:
```js
words(1) => "first"
words(10) => "tenth"
words(11) => "eleventh"
words(122) => "hundred and twenty-second"
words(3501) => "three thousand five hundred and first"
```
