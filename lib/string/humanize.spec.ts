import { expect } from "chai";
import humanizeString from "./humanize";

describe("humanizeString", function() {
    it("should handle a single lower case letter", function() {
        expect(humanizeString("a"))
            .to.equal("A");
    });

    it("should handle a single upper case letter", function() {
        expect(humanizeString("A"))
            .to.equal("A");
    });

    it("should handle only special characters", function() {
        expect(humanizeString("?)@"))
            .to.equal("");
    });

    it("should handle only one special character", function() {
        expect(humanizeString("?"))
            .to.equal("");
    });

    it("should handle an empty string", function() {
        expect(humanizeString("?"))
            .to.equal("");
    });

    describe("pascal case", function() {
        it("should handle pascal case", function() {
            expect(humanizeString("PascalCaseInputStringIsTurnedIntoSentence"))
                .to.equal("Pascal case input string is turned into sentence");
        });

        it("should handle pascal case with 'I'", function() {
            expect(humanizeString("WhenIUseAnInputAHere"))
                .to.equal("When I use an input a here");
        });

        it("should handle pascal case with a number at the start", function() {
            expect(humanizeString("10IsInTheBeginning"))
                .to.equal("10 is in the beginning");
        });

        it("should handle pascal case with a number in the middle", function() {
            expect(humanizeString("NumberIsFollowedByLowerCase5th"))
                .to.equal("Number is followed by lower case 5th");
        });

        it("should handle pascal case with a number at the end", function() {
            expect(humanizeString("NumberIsAtTheEnd100"))
                .to.equal("Number is at the end 100");
        });

        it("should handle pascal case single word at the start", function() {
            expect(humanizeString("XIsTheFirstWordInTheSentence"))
                .to.equal("X is the first word in the sentence");
        });

        it("should handle pascal case with a space", function() {
            expect(humanizeString("XIsTheFirstWordInTheSentence ThenThereIsASpace"))
                .to.equal("X is the first word in the sentence then there is a space");
        });

        it("should handle pascal case with special characters", function() {
            expect(humanizeString("ContainsSpecial?)@Characters"))
                .to.equal("Contains special characters");
        });
    });

    describe("underscores and dashes", function() {
        it("should handle an underscore separated value", function() {
            expect(humanizeString("Underscored_input_string_is_turned_into_sentence"))
                .to.equal("Underscored input string is turned into sentence");
        });

        it("should handle an underscore separated value with capitalisation", function() {
            expect(humanizeString("Underscored_input_String_is_turned_INTO_sentence"))
                .to.equal("Underscored input String is turned INTO sentence");
        });

        it("should handle value with dashes and spaces", function() {
            expect(humanizeString("TEST 1 - THIS IS A TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with space then dash", function() {
            expect(humanizeString("TEST 1 -THIS IS A TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with dash then space", function() {
            expect(humanizeString("TEST 1- THIS IS A TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with underscore then space", function() {
            expect(humanizeString("TEST 1_ THIS IS A TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with space then underscore", function() {
            expect(humanizeString("TEST 1 _THIS IS A TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with underscores and spaces", function() {
            expect(humanizeString("TEST 1 _ THIS IS A TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with underscore and space separation", function() {
            expect(humanizeString("TEST 1 - THIS_IS_A_TEST"))
                .to.equal("TEST 1 THIS IS A TEST");
        });

        it("should handle a value with varying capitalisation", function() {
            expect(humanizeString("TEST 1 - THIS_is_A_Test"))
                .to.equal("TEST 1 THIS is A test");
        });
    });

    describe("acronyms", function() {
        it("should not wordify an acronym", function() {
            expect(humanizeString("HTML"))
                .to.equal("HTML");
        });

        it("should not wordify an acronym surrounded by pascal case", function() {
            expect(humanizeString("TheHTMLLanguage"))
                .to.equal("The HTML language");
        });

        it("should not wordify an acronym before pascal case", function() {
            expect(humanizeString("HTMLIsTheLanguage"))
                .to.equal("HTML is the language");
        });

        it("should not wordify an acronym after pascal case with spaces", function() {
            expect(humanizeString("TheLanguage IsHTML"))
                .to.equal("The language is HTML");
        });

        it("should not wordify an acronym after pascal case", function() {
            expect(humanizeString("TheLanguageIsHTML"))
                .to.equal("The language is HTML");
        });

        it("should not wordify an acronym then a number", function() {
            expect(humanizeString("HTML5"))
                .to.equal("HTML 5");
        });

        it("should not wordify a number then an acronym", function() {
            expect(humanizeString("1HTML"))
                .to.equal("1 HTML");
        });
    })
})
