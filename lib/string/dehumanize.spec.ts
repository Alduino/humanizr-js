import { expect } from "chai"
import dehumanizeString from "./dehumanize";

describe("dehumanizeString", function() {
    it("should camelise a pascal case sentence", function() {
        expect(dehumanizeString("Pascal case sentence is camelised"))
            .to.equal("PascalCaseSentenceIsCamelised");
    });

    it("should camelise a title case sentence", function() {
        expect(dehumanizeString("Title Case Sentence Is Camelised"))
            .to.equal("TitleCaseSentenceIsCamelised");
    });

    it("should camelise a mixed case sentence", function() {
        expect(dehumanizeString("Mixed case sentence Is Camelised"))
            .to.equal("MixedCaseSentenceIsCamelised");
    });

    it("should camelise a lower case sentence", function() {
        expect(dehumanizeString("lower case sentence is camelised"))
            .to.equal("LowerCaseSentenceIsCamelised");
    });

    it("should not touch an already dehumanised string", function() {
        expect(dehumanizeString("AlreadyDehumanisedStringIsUntouched"))
            .to.equal("AlreadyDehumanisedStringIsUntouched");
    });

    it("should not touch an empty string", function() {
        expect(dehumanizeString(""))
            .to.equal("");
    });

    it("should remove special characters", function() {
        expect(dehumanizeString("A special character is removed?"))
            .to.equal("ASpecialCharacterIsRemoved");
    });

    it("should remove a special character after a space", function() {
        expect(dehumanizeString("A special character is removed after a space ?"))
            .to.equal("ASpecialCharacterIsRemovedAfterASpace");
    });

    it("should remove internal special characters", function() {
        expect(dehumanizeString("Internal special characters ?)@ are removed"))
            .to.equal("InternalSpecialCharactersAreRemoved");
    });
})
