import { expect } from "chai";
import truncate, {fixedLength, fixedNumberOfCharacters, fixedNumberOfWords} from "./index";
import TruncateFrom from "./TruncateFrom";

describe("truncate", function() {
    describe("FixedLengthTruncator", function() {
        it("should return null when null is passed", function() {
            expect(truncate(null, 10))
                .to.equal(null);
        });

        it("should return an empty string when that is passed", function() {
            expect(truncate("", 10))
                .to.equal("");
        });

        it("should return single character when that is passed", function() {
            expect(truncate("a", 10))
                .to.equal("a");
        });

        it("should truncate when input is longer than length", function() {
            expect(truncate("Text longer than truncate length", 10))
                .to.equal("Text long…");
        });

        it("should not do anything when input is the same length", function() {
            expect(truncate("Text with length equal to truncate length", 41))
                .to.equal("Text with length equal to truncate length");
        });

        it("should not do anything when input is shorter", function() {
            expect(truncate("Text smaller than truncate length", 34))
                .to.equal("Text smaller than truncate length");
        });

        it("should handle other truncation strings", function() {
            expect(truncate("Text longer than truncate length", 10, "..."))
                .to.equal("Text lo...");
        });

        it("should handle truncating from start", function() {
            expect(truncate("Text longer than truncate length", 10, null, fixedLength, TruncateFrom.Start))
                .to.equal("…te length");
        });

        it("should handle many spaces", function() {
            expect(truncate("Text   with many spaces", 10))
                .to.equal("Text   wi…");
        });
    });

    describe("FixedNumberOfCharactersTruncator", function() {
        it("should return null when null is passed", function() {
            expect(truncate(null, 10, null, fixedNumberOfCharacters))
                .to.equal(null);
        });

        it("should return an empty string when that is passed", function() {
            expect(truncate("", 10, null, fixedNumberOfCharacters))
                .to.equal("");
        });

        it("should return single character when that is passed", function() {
            expect(truncate("a", 10, null, fixedNumberOfCharacters))
                .to.equal("a");
        });

        it("should truncate when input is longer than length", function() {
            expect(truncate("Text with more characters than truncate length", 10, null, fixedNumberOfCharacters))
                .to.equal("Text with m…");
        });

        it("should truncate when input is same as length", function() {
            expect(truncate("Text with number of characters equal to truncate length", 47, null, fixedNumberOfCharacters))
                .to.equal("Text with number of characters equal to truncate length");
        });

        it("should truncate when input is shorter", function() {
            expect(truncate("Text with less characters than truncate length", 41, null, fixedNumberOfCharacters))
                .to.equal("Text with less characters than truncate length");
        });

        it("should handle other truncation strings", function() {
            expect(truncate("Text with more characters than truncate length", 10, "...", fixedNumberOfCharacters))
                .to.equal("Text wit...");
        });

        it("should handle truncating from start", function() {
            expect(truncate("Text with more characters than truncate length", 10, null, fixedNumberOfCharacters, TruncateFrom.Start))
                .to.equal("…ate length");
        });

        it("should handle many spaces", function() {
            expect(truncate("Text   with many spaces", 10, null, fixedNumberOfCharacters))
                .to.equal("Text   with m…");
        });
    });

    describe("FixedNumberOfWordsTruncator", function() {
        it("should return null when null is passed", function() {
            expect(truncate(null, 10, null, fixedNumberOfCharacters))
                .to.equal(null);
        });

        it("should return an empty string when that is passed", function() {
            expect(truncate("", 10, null, fixedNumberOfCharacters))
                .to.equal("");
        });

        it("should return single character when that is passed", function() {
            expect(truncate("a", 10, null, fixedNumberOfCharacters))
                .to.equal("a");
        });

        it("should truncate when input is longer than length", function() {
            expect(truncate("Text with more words than truncate length", 4, null, fixedNumberOfWords))
                .to.equal("Text with more words…");
        });

        it("should truncate when input is same as length", function() {
            expect(truncate("Text with number of words equal to truncate length", 9, null, fixedNumberOfWords))
                .to.equal("Text with number of words equal to truncate length");
        });

        it("should truncate when input is shorter", function() {
            expect(truncate("Text with less words than truncate length", 8, null, fixedNumberOfWords))
                .to.equal("Text with less words than truncate length");
        });

        it("should handle other whitespace characters", function() {
            expect(truncate("Words are\nsplit\rby\twhitespace", 4, null, fixedNumberOfWords))
                .to.equal("Words are\nsplit\rby…");
        });

        it("should handle other truncation strings", function() {
            expect(truncate("Text with more words than truncate length", 4, "...", fixedNumberOfWords))
                .to.equal("Text with more words...");
        });

        it("should handle truncating from start", function() {
            expect(truncate("Text with more words than truncate length", 4, null, fixedNumberOfWords, TruncateFrom.Start))
                .to.equal("…words than truncate length");
        });

        it("should handle many spaces", function() {
            expect(truncate("Text   with  many spaces", 3, null, fixedNumberOfWords))
                .to.equal("Text   with  many…");
        });
    });
});
