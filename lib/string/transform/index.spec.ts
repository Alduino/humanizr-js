import {expect} from "chai";
import transform, {toLowerCase, toSentenceCase, toTitleCase, toUpperCase} from "./index";

describe("transform", function() {
    describe("title case", function() {
        it("should convert lower case to title case", function() {
            expect(transform("lower case statement", toTitleCase))
                .to.equal("Lower Case Statement");
        });

        it("should convert sentence case to title case", function() {
            expect(transform("sentence casing", toTitleCase))
                .to.equal("Sentence Casing");
        });

        it("should keep upper case words", function() {
            expect(transform("honours UPPER case", toTitleCase))
                .to.equal("Honours UPPER Case");
        });

        it("should correct invalid casing", function() {
            expect(transform("INvalid caSEs arE corrected", toTitleCase))
                .to.equal("Invalid Cases Are Corrected");
        });

        it("should handle one letter words", function() {
            expect(transform("Can deal w 1 letter words as i do", toTitleCase))
                .to.equal("Can Deal W 1 Letter Words As I Do");
        });

        it("should keep spaces around words", function() {
            expect(transform("  random spaces   are HONOURED    too ", toTitleCase))
                .to.equal("  Random Spaces   Are HONOURED    Too ");
        });

        it("shouldn't touch existing title case", function() {
            expect(transform("Title Case", toTitleCase))
                .to.equal("Title Case");
        });

        it("should handle apostrophes", function() {
            expect(transform("apostrophe's aren't capitalised", toTitleCase))
                .to.equal("Apostrophe's Aren't Capitalised");
        });

        it("should handle commas", function() {
            expect(transform("titles with, commas work too", toTitleCase))
                .to.equal("Titles With, Commas Work Too");
        });
    });

    describe("lower case", function() {
        it("shouldn't touch existing lower case", function() {
            expect(transform("lower case statement", toLowerCase))
                .to.equal("lower case statement");
        });

        it("should handle sentence case", function() {
            expect(transform("Sentence casing", toLowerCase))
                .to.equal("sentence casing");
        });

        it("should handle upper case", function() {
            expect(transform("No honour for UPPER case", toLowerCase))
                .to.equal("no honour for upper case");
        });

        it("should handle title case", function() {
            expect(transform("Title Case", toLowerCase))
                .to.equal("title case");
        });
    });

    describe("sentence case", function() {
        it("should handle lower case", function() {
            expect(transform("lower case statement", toSentenceCase))
                .to.equal("Lower case statement");
        });

        it("shouldn't touch sentence case", function() {
            expect(transform("Sentence casing", toSentenceCase))
                .to.equal("Sentence casing");
        });

        it("should keep upper case words", function() {
            expect(transform("honours UPPER case", toSentenceCase))
                .to.equal("Honours UPPER case");
        });
    });

    describe("upper case", function() {
        it("should handle lower case", function() {
            expect(transform("lower case statement", toUpperCase))
                .to.equal("LOWER CASE STATEMENT");
        });

        it("should handle sentence case", function() {
            expect(transform("Sentence casing", toUpperCase))
                .to.equal("SENTENCE CASING");
        });

        it("should handle title case", function() {
            expect(transform("Title Case", toUpperCase))
                .to.equal("TITLE CASE");
        });
    });
});
