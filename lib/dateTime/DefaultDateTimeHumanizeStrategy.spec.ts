import { expect } from "chai";
import DefaultDateTimeHumanizeStrategy from "./DefaultDateTimeHumanizeStrategy";

describe("DefaultDateTimeHumanizeStrategy", function() {
    const {humanize} = new DefaultDateTimeHumanizeStrategy();

    it("should return now for anything less than one second ago", function() {
        expect(humanize(new Date(Date.now() - 500), new Date()))
            .to.equal("now");
    });

    it("should return now for anything less than one second in the future", function() {
        expect(humanize(new Date(Date.now() + 500), new Date()))
            .to.equal("now");
    });

    it("should floor the input", function() {
        expect(humanize(new Date(Date.now() + 999), new Date()))
            .to.equal("now");
    });

    it("should handle one second", function() {
        expect(humanize(new Date(Date.now() + 1000), new Date()))
            .to.equal("in one second");
    });

    it("should handle 30 seconds", function() {
        expect(humanize(new Date(Date.now() + 30000), new Date()))
            .to.equal("in 30 seconds");
    });

    it("should handle minutes", function() {
        expect(humanize(new Date(Date.now() + 60000), new Date()))
            .to.equal("in one minute");
    });

    it("should handle hours", function() {
        expect(humanize(new Date(Date.now() + 3600000), new Date()))
            .to.equal("in one hour");
    });
});
