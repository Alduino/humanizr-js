import { expect } from "chai";
import words from "./words";

describe("words", function() {
    describe("to words", function() {
        it("should handle a single digit", function() {
            expect(words(1))
                .to.equal("one");
        });

        it("should handle a multiple of ten", function() {
            expect(words(10))
                .to.equal("ten");
        });

        it("should handle a teen", function() {
            expect(words(11))
                .to.equal("eleven");
        });

        it("should handle another multiple of ten", function() {
            expect(words(20))
                .to.equal("twenty");
        });

        it("should handle a double digit number", function() {
            expect(words(22))
                .to.equal("twenty-two");
        });

        it("should handle a three digit number", function() {
            expect(words(122))
                .to.equal("one hundred and twenty-two");
        });

        it("should handle a multiple of 100", function() {
            expect(words(100))
                .to.equal("one hundred");
        });

        it("should handle a four digit number", function() {
            expect(words(3501))
                .to.equal("three thousand five hundred and one");
        });

        it("should handle a six digit number", function() {
            expect(words(500505))
                .to.equal("five hundred thousand five hundred and five");
        });
    });

    describe("ordinal", function() {
        it("should handle zero", function() {
            expect(words(0, true))
                .to.equal("zeroth");
        });

        it("should handle 1st", function() {
            expect(words(1, true))
                .to.equal("first");
        });

        it("should handle 2nd", function() {
            expect(words(2, true))
                .to.equal("second");
        });

        it("should handle 3rd", function() {
            expect(words(3, true))
                .to.equal("third");
        });

        it("should handle th", function() {
            expect(words(4, true))
                .to.equal("fourth");
        });

        it("should handle teens", function() {
            expect(words(12, true))
                .to.equal("twelfth");
        });

        it("should handle tens", function() {
            expect(words(23, true))
                .to.equal("twenty-third");
        });

        it("should handle hundreds", function() {
            expect(words(121, true))
                .to.equal("hundred and twenty-first");
        });

        it("should handle thousands", function() {
            expect(words(1021, true))
                .to.equal("thousand and twenty-first");
        });
    });
});
